/**
 * Of course, they are equal!
 */
console.log(0 == [])

/**
 * These are equal too :)
 */
 console.log(0 == '0')


/**
 * Not these, though :(
 */
 console.log('0' == [])

 /**
  * Let's imagine I changed this line.
  */
